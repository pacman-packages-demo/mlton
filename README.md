# MLton

Whole-program, optimizing Standard ML compiler. http://mlton.org/

## Unofficial documentation
* [repology](https://repology.org/project/mlton/versions)
* WikipediA
  * [MLton](https://en.wikipedia.org/wiki/MLton)
  * [*Standard ML*](https://en.wikipedia.org/wiki/Standard_ML)
* [*Standard ML and how I’m compiling it*
  ](https://thebreakfastpost.com/2015/06/10/standard-ml-and-how-im-compiling-it/)
  2015 Chris Cannam

## Projects using neighbour languages
* ML
  * Standard ML
    * Poly/ML
      * [pacman-packages-demo/polyml](https://gitlab.com/pacman-packages-demo/polyml)
    * SML/NJ
      * [pacman-packages-demo/smlnj](https://gitlab.com/pacman-packages-demo/smlnj)
  * OCaml
    * [pacman-packages-demo/ocaml](https://gitlab.com/pacman-packages-demo/ocaml)
    * [ocaml-packages-demo](https://gitlab.com/ocaml-packages-demo)
* Mercury
  * [apt-packages-demo/mercury](https://gitlab.com/apt-packages-demo/mercury)